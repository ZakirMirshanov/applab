from google.cloud import bigquery
from google.oauth2 import service_account
import pandas_gbq

import pycurl
from io import BytesIO
import hashlib
import hmac
import csv
import json
import requests
from requests.structures import CaseInsensitiveDict


# загрузка credentials
credentials = service_account.Credentials.from_service_account_file('bookmaker-ratings-8e32f-692e0d9ceb8c.json')
project_id = 'bookmaker-ratings-8e32f'
client = bigquery.Client(credentials=credentials, project=project_id)

# загрузка данных в DataFrame для последующей передаче по API
df = pandas_gbq.read_gbq("""
   select f.user_id from(
  select distinct userID as user_id 
  from (SELECT  (SELECT value.int_value FROM UNNEST(event_params) 
                 WHERE key = "userId") AS userID
    FROM `bookmaker-ratings-8e32f.analytics_151181977.events_2022*`,
    UNNEST(event_params) AS param
    WHERE _TABLE_SUFFIX BETWEEN '0101' AND '0630' 
      and platform='IOS' or platform='Android'
      AND event_name = "lk_form" 
      and param.key='form_action' 
      and (param.value.string_value='sms-register' or param.value.string_value='sms-no-register'))) f
    --left outer join (select user_id from `bookmaker-ratings-8e32f.analytics_151181977.Users_for_RB`)us 
    --on us.user_id=f.user_id
    --where us.user_id is NULL
 """, project_id="bookmaker-ratings-8e32f", credentials=credentials)

# вставка данных в таблицу Users_for_RB
query_job = client.query("""
   insert into `bookmaker-ratings-8e32f.analytics_151181977.Users_for_RB`

select f.user_id from(
  select distinct userID as user_id 
  from (SELECT  (SELECT value.int_value FROM UNNEST(event_params) 
                 WHERE key = "userId") AS userID
    FROM `bookmaker-ratings-8e32f.analytics_151181977.events_2022*`,
    UNNEST(event_params) AS param
    WHERE _TABLE_SUFFIX BETWEEN '0101' AND '0630' 
      and platform='IOS' or platform='Android'
      AND event_name = "lk_form" 
      and param.key='form_action' 
      and (param.value.string_value='sms-register' or param.value.string_value='sms-no-register'))) f
    left outer join (select user_id from `bookmaker-ratings-8e32f.analytics_151181977.Users_for_RB`)us 
    on us.user_id=f.user_id
    where us.user_id is NULL
 """)

results = query_job.result()

b_obj = BytesIO()
crl = pycurl.Curl()
# Set URL value
crl.setopt(crl.URL, 'https://wiki.python.org/moin/BeginnersGuide')
# Write bytes that are utf-8 encoded
crl.setopt(crl.WRITEDATA, b_obj)
# Perform a file transfer
crl.perform()
# End curl session
crl.close()
# Get the content stored in the BytesIO object (in byte characters)
get_body = b_obj.getvalue()
# Decode the bytes stored in get_body to HTML and print the result
print('Output of GET request:\n%s' % get_body.decode('utf8'))


def encrypt_string(hash_string):
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature


hash_string = '{"amount":"100", "phone":"79850547214", "uid":"991", "комментарий":"комментарий", "checksum":"6ff6542f5a19587847eb269ebb9e03d5acf92a0c1f9171262721b26ce3b9e508"}'
sha_signature = encrypt_string(hash_string)
print(sha_signature)



my = '{"amount":"100", "phone":"79850547214", "uid":"991"}'.encode()
key = '4125065e48e14a003f0823bc53b1bcb4'.encode()
#key = bytes(key_1,'UTF-8')
#key1=key.encode()
h = hmac.new( key, my, hashlib.sha256 )
print( h.hexdigest() )



url = 'https://api.rbplus.ru/api/v1/partners/bonuses'

#headers = CaseInsensitiveDict()
#headers["Content-Type"] = "application/json"
#headers["Accept"] = "application/json"
#headers["X-API-KEY"] = "fed497559c50ef54b1ff728e64536149"
headers = {
    'Accept':'application/json','Content-Type':'application/json','X-API-KEY':'fed497559c50ef54b1ff728e64536149'
}
data = '{"amount":"100", "phone":"79850547214", "uid":"991", "comment":"asap", "checksum":"4e12fa608ee649c28451b7be255875bcec82da430e20029206e3ad3cc60a30f1"}'
resp = requests.post(url, headers=headers, data=data)

print(resp.status_code)
print(headers)

crl = pycurl.Curl()
crl.setopt(crl.HTTPHEADER, ['Accept:application/json','Content-Type:application/json','X-API-KEY:fed497559c50ef54b1ff728e64536149'])
crl.setopt(crl.URL, 'https://api.rbplus.ru/api/v1/partners/bonuses')
data = {"amount":"100", "phone":"78888888888", "uid":"111", "comment":"asap", "checksum":"d6bd0d22dbcf9f27806b4bf5d930c3ef1a07b49c5c80b5dea3aee5581f100c1e"}
pf = urlencode(data)
crl.setopt(crl.POSTFIELDS, pf)
crl.perform()
crl.close()


# конвертация csv  в json
def csv_to_json(csvFilePath, jsonFilePath):
    jsonArray = []

    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
        for row in csvReader:
            jsonArray.append(row)
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
        jsonString = json.dumps(jsonArray, indent=4)
        jsonf.write(jsonString)


csvFilePath = r'ama.csv'
jsonFilePath = r'ama.json'
csv_to_json(csvFilePath, jsonFilePath)

# создаем список словарей из json
with open('ama.json') as d:
    dictData = json.load(d)
    print(dictData)
# цикл, который отправляет каждый словарь в API
for i in range(len(dictData)):
    d = dictData[i]['comment']
    del dictData[i]['comment']
    dictData[i]['uid'] = 100 + i
    my = str(dictData[i]).encode()
    key = '4125065e48e14a003f0823bc53b1bcb4'.encode()
    h = hmac.new(key, my, hashlib.sha256)
    dictData[i]['checksum'] = h.hexdigest()
    dictData[i]['comment'] = d

print(dictData)
